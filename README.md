# Introduction to git with a gitlab After-flavor

[Slides](https://docs.google.com/presentation/d/1mBk6aHE7XgGYzCUm31BhEFvaRvKV_On7NCk1v9X7GQM/edit?usp=sharing)

[Exercise 1: git init, add, status, commit, reset, diff, log](https://gitlab.com/dPliakos/git_introduction/blob/master/exercises/ex1.md)

[Exercise 2: branches, merging and conflicts](https://gitlab.com/dPliakos/git_introduction/blob/master/exercises/ex2.md)

[Exercise 3: rebasing](https://gitlab.com/dPliakos/git_introduction/blob/master/exercises/ex3.md)

[Exercise 4: Github repositories, git remote, push, pull](https://gitlab.com/dPliakos/git_introduction/blob/master/exercises/ex4.md)

[Exercise 5: forking, upstream and pull requests](https://gitlab.com/dPliakos/git_introduction/blob/master/exercises/ex5.md)
